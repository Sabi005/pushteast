//
//  main.m
//  PushPopDelegate
//
//  Created by MANOJ on 23/11/15.
//  Copyright (c) 2015 Hiworth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
